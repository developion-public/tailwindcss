const DevelopionCLI = require('./developion-cli')

const runCLI = async () => {
	const cli = new DevelopionCLI()

	try {
		await cli.run()
	} catch (error) {
		cli.logger.error(error);
		process.exit(2)
	}
}

module.exports = runCLI
