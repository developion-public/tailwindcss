const fs = require('fs')
const path = require('path')
const util = require('util')
const { exec } = require('child_process')

const { program, Option } = require('commander')

class TailwindCLI {
	constructor() {
		this.colors = this.createColors()
		this.logger = this.getLogger()

		// Initialize program
		this.program = program
		this.program.name('developion-tailwind')
		this.program.configureOutput({
			writeErr: this.logger.error,
			outputError: (str, write) =>
				write(`Error: ${this.capitalizeFirstLetter(str.replace(/^error:/, '').trim())}`),
		})
	}

	createColors(useColor) {
		const { createColors, isColorSupported } = require('colorette')

		let shouldUseColor

		if (useColor) {
			shouldUseColor = useColor
		} else {
			shouldUseColor = isColorSupported
		}

		return { ...createColors({ useColor: shouldUseColor }), isColorSupported: shouldUseColor }
	}

	getLogger() {
		return {
			error: val =>
				console.error(`[developion-tailwind] ${this.colors.red(util.format(val))}`),
			warn: val => console.warn(`[developion-tailwind] ${this.colors.yellow(val)}`),
			info: val => console.info(`[developion-tailwind] ${this.colors.cyan(val)}`),
			success: val => console.log(`[developion-tailwind] ${this.colors.green(val)}`),
			log: val => console.log(`[developion-tailwind] ${val}`),
			raw: val => console.log(val),
		}
	}

	capitalizeFirstLetter(str) {
		if (typeof str !== 'string') {
			return ''
		}

		return str.charAt(0).toUpperCase() + str.slice(1)
	}

	toKebabCase(str) {
		return str.replace(/([a-z0-9])([A-Z])/g, '$1-$2').toLowerCase()
	}

	checkPackageExists(packageName) {
		if (process.versions.pnp) {
			return true
		}

		let dir = __dirname

		do {
			try {
				if (fs.statSync(path.join(dir, 'node_modules', packageName)).isDirectory()) {
					return true
				}
			} catch (_error) {
				// Nothing
			}
		} while (dir !== (dir = path.dirname(dir)))

		return false
	}

	loadJSONFile(pathToFile, handleError = true) {
		let result, resultRaw

		try {
			resultRaw = fs.readFileSync(pathToFile)
			result = JSON.parse(resultRaw)
		} catch (error) {
			this.logger.error(error)
		}

		return result
	}

	async run() {
		let cssListJSON = process.argv[2]
		if (!cssListJSON) {
			this.logger.error('css list required')
			return
		}
		console.log("\n")
		const cssList = this.loadJSONFile(cssListJSON)

		cssList.forEach(css => {
			this.logger.info(`Rendering ${css}`)
			// exec(`npx tailwindcss -i ./src/css/${css} -o ./dist/css/${css}`)
		})
	}
}

const cli = new TailwindCLI
cli.run()
